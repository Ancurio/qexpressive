#include "mainwindow.h"

#include <QLabel>
#include <QLineEdit>
#include <QHBoxLayout>
#include <QVBoxLayout>

#include "regexengine.h"

struct MainWindowPriv {
	QLabel *lblResult;
	QLineEdit *editPattern;
	QLineEdit *editInput;

	RegexEngine re;

	void updateMatch(const QString &text) {
		if (!re.reValid())
			return;

		QString display;

		switch (re.matches(text.toStdU32String())) {
		case RegexEngine::FullMatch    : display = "full match";    break;
		case RegexEngine::PartialMatch : display = "partial match"; break;
		case RegexEngine::NoMatch      : display = "no match";      break;
		}

		lblResult->setText(display);
	}
};

MainWindow::MainWindow(QWidget *parent) : QWidget(parent) {
	p = new MainWindowPriv;

	p->lblResult = new QLabel();
	p->editPattern = new QLineEdit();
	p->editInput = new QLineEdit();

	auto vboxMain = new QVBoxLayout();

	vboxMain->addWidget(new QLabel("Pattern:"));
	vboxMain->addWidget(p->editPattern);
	vboxMain->addWidget(new QLabel("Input:"));
	vboxMain->addWidget(p->editInput);

	vboxMain->addWidget(p->lblResult);

	setLayout(vboxMain);

	connect(p->editPattern, &QLineEdit::textChanged, this, &MainWindow::onPatternChanged);
	connect(p->editInput, &QLineEdit::textChanged, this, &MainWindow::onInputChanged);
}

MainWindow::~MainWindow() {
	delete p;
}

void MainWindow::onPatternChanged(const QString &str) {
	try {
		p->re.setPattern(str.toStdU32String());
	} catch (const std::string &msg) {
		p->lblResult->setText(QString("Error: ") + msg.c_str());
		return;
	}

	p->updateMatch(p->editInput->text());
}

void MainWindow::onInputChanged(const QString &str) {
	p->updateMatch(str);
}
