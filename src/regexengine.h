#ifndef REGEXENGINE_H
#define REGEXENGINE_H

#include <string>

struct RegexEnginePriv;

class RegexEngine {
public:
	RegexEngine();
	~RegexEngine();

	enum MatchResult {
		FullMatch,
		PartialMatch,
		NoMatch,
	};

	/* throws std::string with error message */
	void setPattern(const std::u32string &pattern);
	MatchResult matches(const std::u32string &str) const;
	bool reValid() const;
private:
	RegexEnginePriv *p;
};

#endif // REGEXENGINE_H
