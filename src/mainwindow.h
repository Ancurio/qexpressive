#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>

struct MainWindowPriv;
class QString;

class MainWindow : public QWidget
{
	Q_OBJECT
public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private slots:
	void onPatternChanged(const QString &);
	void onInputChanged(const QString &);

private:
	MainWindowPriv *p;
};

#endif // MAINWINDOW_H
