#ifndef CHAR32_TRAITS_H
#define CHAR32_TRAITS_H

#include <boost/xpressive/xpressive.hpp>
#include <boost/xpressive/traits/null_regex_traits.hpp>
#include <boost/xpressive/traits/cpp_regex_traits.hpp>

namespace xpr = boost::xpressive;

/* Reference on making xpressive work with u32string:
 * http://www.slideshare.net/egtra/char32txpressive
 *
 * stuff's way over my head atm */
struct char32_regex_traits : xpr::null_regex_traits<char32_t> {
	struct locale_type {};

	enum char_class {
		InvalidClass = 0,
		Lu = (1 << 0),
		LI = (1 << 1),
		/* ... */
		NewLine = (1 << 2),
	};

	typedef boost::uint_value_t<NewLine>::least char_class_type;

	/// Initialize a null_regex_traits object.
	///
	char32_regex_traits(locale_type = locale_type())
	{
	}

	/// Checks two null_regex_traits objects for equality
	///
	/// \return true.
//	bool operator ==(null_regex_traits<char_type> const &that) const
//	{
//		detail::ignore_unused(that);
//		return true;
//	}

	/// Checks two null_regex_traits objects for inequality
	///
	/// \return false.
//	bool operator !=(null_regex_traits<char_type> const &that) const
//	{
//		detail::ignore_unused(that);
//		return false;
//	}

	/// Convert a char to a Elem
	///
	/// \param ch The source character.
	/// \return Elem(ch).
//	char_type widen(char ch) const
//	{
//		return char_type(ch);
//	}

	/// Returns a hash value for a Elem in the range [0, UCHAR_MAX]
	///
	/// \param ch The source character.
	/// \return a value between 0 and UCHAR_MAX, inclusive.
//	static unsigned char hash(char_type ch)
//	{
//		return static_cast<unsigned char>(ch);
//	}

	/// No-op
	///
	/// \param ch The source character.
	/// \return ch
//	static char_type translate(char_type ch)
//	{
//		return ch;
//	}

	/// No-op
	///
	/// \param ch The source character.
	/// \return ch
//	static char_type translate_nocase(char_type ch)
//	{
//		return ch;
//	}

	/// Checks to see if a character is within a character range.
	///
	/// \param first The bottom of the range, inclusive.
	/// \param last The top of the range, inclusive.
	/// \param ch The source character.
	/// \return first <= ch && ch <= last.
//	static bool in_range(char_type first, char_type last, char_type ch)
//	{
//		return first <= ch && ch <= last;
//	}

	/// Checks to see if a character is within a character range.
	///
	/// \param first The bottom of the range, inclusive.
	/// \param last The top of the range, inclusive.
	/// \param ch The source character.
	/// \return first <= ch && ch <= last.
	/// \attention Since the null_regex_traits does not do case-folding,
	/// this function is equivalent to in_range().
//	static bool in_range_nocase(char_type first, char_type last, char_type ch)
//	{
//		return first <= ch && ch <= last;
//	}

	/// Returns a sort key for the character sequence designated by the iterator range [F1, F2)
	/// such that if the character sequence [G1, G2) sorts before the character sequence [H1, H2)
	/// then v.transform(G1, G2) < v.transform(H1, H2).
	///
	/// \attention Not currently used
//	template<typename FwdIter>
//	static string_type transform(FwdIter begin, FwdIter end)
//	{
//		return string_type(begin, end);
//	}

	/// Returns a sort key for the character sequence designated by the iterator range [F1, F2)
	/// such that if the character sequence [G1, G2) sorts before the character sequence [H1, H2)
	/// when character case is not considered then
	/// v.transform_primary(G1, G2) < v.transform_primary(H1, H2).
	///
	/// \attention Not currently used
//	template<typename FwdIter>
//	static string_type transform_primary(FwdIter begin, FwdIter end)
//	{
//		return string_type(begin, end);
//	}

	/// Returns a sequence of characters that represents the collating element
	/// consisting of the character sequence designated by the iterator range [F1, F2).
	/// Returns an empty string if the character sequence is not a valid collating element.
	///
	/// \attention Not currently used
//	template<typename FwdIter>
//	static string_type lookup_collatename(FwdIter begin, FwdIter end)
//	{
//		detail::ignore_unused(begin);
//		detail::ignore_unused(end);
//		return string_type();
//	}

	/// The null_regex_traits does not have character classifications, so lookup_classname()
	/// is unused.
	///
	/// \param begin not used
	/// \param end not used
	/// \param icase not used
	/// \return static_cast\<char_class_type\>(0)
//	template<typename FwdIter>
//	static char_class_type lookup_classname(FwdIter begin, FwdIter end, bool icase)
//	{
//		detail::ignore_unused(begin);
//		detail::ignore_unused(end);
//		detail::ignore_unused(icase);
//		return 0;
//	}

	/// The null_regex_traits does not have character classifications, so isctype()
	/// is unused.
	///
	/// \param ch not used
	/// \param mask not used
	/// \return false
//	static bool isctype(char_type ch, char_class_type mask)
//	{
//		detail::ignore_unused(ch);
//		detail::ignore_unused(mask);
//		return false;
//	}

	/// The null_regex_traits recognizes no elements as digits, so value() is unused.
	///
	/// \param ch not used
	/// \param radix not used
	/// \return -1
	static int value(char32_t ch, int radix)
	{
		if (radix != 10)
			return -1;

		if (ch < '0' || ch > '9')
			return -1;

		return ch - '0';
	}

	/// Not used
	///
	/// \param loc not used
	/// \return loc
//	static locale_type imbue(locale_type loc)
//	{
//		return loc;
//	}

	/// Returns locale_type().
	///
	/// \return locale_type()
//	static locale_type getloc()
//	{
//		return locale_type();
//	}
};

#endif // CHAR32_TRAITS_H
