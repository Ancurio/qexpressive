#include "regexengine.h"

#include <string>
#include <iostream>
#include <boost/xpressive/xpressive.hpp>

#include "char32_traits.h"

namespace xpr = boost::xpressive;


namespace Property {
boost::proto::terminal<boost::xpressive::detail::posix_charset_placeholder>::type const L = {{"L", false}};
}

//typedef xpr::regex_compiler<std::u32string::const_iterator, xpr::cpp_regex_traits<char32_t> > U32Comp;
typedef xpr::regex_compiler<std::u32string::const_iterator, char32_regex_traits> U32Comp;
typedef xpr::basic_regex<std::u32string::const_iterator> U32Re;

struct RegexEnginePriv {
	U32Comp comp;
	U32Re re;
	bool valid;
};

RegexEngine::RegexEngine() {
	p = new RegexEnginePriv;
	p->valid = true;
}

RegexEngine::~RegexEngine() {
	delete p;
}

void RegexEngine::setPattern(const std::u32string &pattern) {
	try {
		p->re = p->comp.compile(pattern);
	} catch (const xpr::regex_error &exc) {
		std::string msg(exc.what());
		p->re = U32Re();
		p->valid = false;
		throw msg;
	}

	p->valid = true;
}

RegexEngine::MatchResult RegexEngine::matches(const std::u32string &str) const {
	if (xpr::regex_match(str, p->re)) {
		return FullMatch;
	}

	if (xpr::regex_search(str, p->re)) {
		return PartialMatch;
	}

	return NoMatch;
}

bool RegexEngine::reValid() const {
	return p->valid;
}
