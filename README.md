Abhängigkeiten:
* boost (nur Header)
* Qt5 (core gui widgets)
* C++11

Referenzen:

Xpressive mit char32_t / u32string:  
http://www.slideshare.net/egtra/char32txpressive (jap)  
http://www.boost.org/doc/libs/1_61_0/doc/html/xpressive/reference.html#header.boost.xpressive.regex_traits_hpp  
http://boost-sandbox.sourceforge.net/libs/xpressive/doc/html/boost_xpressive/user_s_guide/localization_and_regex_traits.html  
http://stackoverflow.com/questions/33708892/why-is-there-no-definition-for-stdregex-traitschar32-t-and-thus-no-stdbas  
http://en.cppreference.com/w/cpp/regex/regex_traits/isctype

Dynamische Regexp in Xpressive:  
http://www.boost.org/doc/libs/1_55_0/doc/html/xpressive/user_s_guide.html#boost_xpressive.user_s_guide.creating_a_regex_object.dynamic_regexes  

UTF Transkodierung:  
http://www.boost.org/doc/libs/1_42_0/libs/serialization/doc/codecvt.html  
http://stackoverflow.com/questions/31302506/stdu32string-conversion-to-from-stdstring-and-stdu16string  

char32_t Literale:  
http://en.cppreference.com/w/cpp/language/string_literal  

http://stackoverflow.com/questions/7232710/convert-between-string-u16string-u32string  

Unicode Normalisation:  
http://unicode.org/faq/normalization.html  

Allgemeines Unicode:  
http://cppwhispers.blogspot.de/2012/11/unicode-and-your-application-1-of-n.html  

ECMAScript (Regexp Grammatik):  
http://en.cppreference.com/w/cpp/regex/ecmascript  
https://www.gnu.org/software/grep/manual/html_node/Character-Classes-and-Bracket-Expressions.html  
